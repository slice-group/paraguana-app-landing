$(document).ready(function(){	

	var home = parseInt($('#home').css('height').split('px')[0]) - 300;
	var features = parseInt($('#features').css('height').split('px')[0]) + 300;
	var place = parseInt($('#place').css('height').split('px')[0]) + 850;
	var cont_home = 0;
	var cont_features = 0;
	var cont_place = 0;
	var cont_footer = 0;

	$(window).bind('scroll', function() {
		if ($(window).scrollTop() < home) {
			if(cont_home == 0){
	    	$(".phone-screen-1").animate({
	    		opacity: "1"
	    	})
	    	$(".phone-screen-2").animate({
					opacity: "0"
    		})
	    	cont_home = 1;
	    	cont_features = 0;
	    	cont_place = 0;
	    	cont_footer = 0;
			}
	  } 
	  if ($(window).scrollTop() > home && $(window).scrollTop() < features) {
	  	if(cont_features == 0){	  
	    	$(".phone-screen-1").animate({
	    		opacity: "0"
	    	})
	    	$(".phone-screen-2").animate({
					opacity: "1"
    		})
    		$(".phone-screen-3").animate({
					opacity: "0"
    		})
	    	cont_home = 0;
	    	cont_features = 1;
	    	cont_place = 0;
	    	cont_footer = 0;
	  	}
	  }
	  if ($(window).scrollTop() > features && $(window).scrollTop() < place) {
	  	if(cont_place == 0){
		    $(".phone-screen-2").animate({
	    		opacity: "0"
	    	})
	    	$(".phone-screen-3").animate({
					opacity: "1"
    		})
		    $(".phone-image").css({
		      position : "fixed",
		      top: "100px"
		    });
		    cont_home = 0;
		    cont_features = 0;
		    cont_place = 1;
		    cont_footer = 0;
		  }	 
	  }
	  if ($(window).scrollTop() > place) {
	  	if(cont_footer == 0){
	  		console.log("stop");
	  		$(".phone-image").css({
		      position : "absolute",
		      top: ($(window).scrollTop()+30)+"px"
		    });
	  		cont_home = 0;
		    cont_features = 0;
		    cont_place = 0;	
		    cont_footer = 1;
	  	} 
	    
	  }
	});
});